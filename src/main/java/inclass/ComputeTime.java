package inclass;

import java.util.Scanner;

public class ComputeTime {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of seconds");

        int inputSeconds = input.nextInt();

        int hours = inputSeconds / 3600;
        int minutes = (inputSeconds % 3600) / 60;
        int seconds = (minutes % 60) / 60;

        System.out.println("There are " + hours + " hours, " + minutes + " minutes, and " + seconds + " seconds" +
             " in " + inputSeconds + " seconds");

    }

}
