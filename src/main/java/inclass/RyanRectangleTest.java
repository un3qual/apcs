package inclass;

public class RyanRectangleTest {

    RyanRectangle rect1 = new RyanRectangle(4, 40);
    RyanRectangle rect2 = new RyanRectangle(3.5, 35.9);

    public static void main(String[] args) {
        RyanRectangle rect1 = new RyanRectangle();
        rect1.setWidth(4);
        rect1.setHeight(40);
        RyanRectangle rect2 = new RyanRectangle(3.5, 35.9);
        System.out.format("Rect1: " +
                "   Width, Height: %f, %f \n" +
                "   Area, Perimeter: %f, %f \n \n", rect1.getWidth(), rect1.getHeight(), rect1.getArea(), rect1.getPerimeter());

        System.out.format("Rect2: " +
                "   Width, Height: %f, %f \n" +
                "   Area, Perimeter: %f, %f",  rect2.getWidth(), rect2.getHeight(), rect2.getArea(), rect2.getPerimeter());
    }
}
