package inclass.ObjectsEx;

public class Cat {

    int numOfFeet = 4;
    String breed = "Not Set";
    int age = 0;
    int eyeCount = 2;
    boolean gender = true;
    boolean isAwake = true;
    // Male = true, female = false

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getEyeCount() {
        return eyeCount;
    }

    public void setEyeCount(int eyeCount) {
        this.eyeCount = eyeCount;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getNumOfFeet() {

        return numOfFeet;
    }

    public void setNumOfFeet(int numOfFeet) {
        this.numOfFeet = numOfFeet;
    }

    public boolean isAwake() {
        return isAwake;
    }

    public void setIsAwake(boolean isAwake) {
        this.isAwake = isAwake;
    }

    /**
     * @param numOfFeet
     * @param breed
     * @param age
     * @param eyeCount
     * @param gender
     * @param isAwake
     */
    Cat(int numOfFeet, String breed, int age, int eyeCount, boolean gender, boolean isAwake) {
        this.numOfFeet = numOfFeet;
        this.breed = breed;
        this.age = age;
        this.eyeCount = eyeCount;
        this.gender = gender;
        this.isAwake = isAwake;
    }
    Cat() {

    }


}
