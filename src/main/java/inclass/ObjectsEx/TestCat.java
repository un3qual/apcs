package inclass.ObjectsEx;

import inclass.ObjectsEx.Cat;
public class TestCat {
    public static Cat Joe = new Cat(4, "Tabby", 3, 2, true, false);
    public static void main(String[] args) {

        Cat Bob = new Cat();
        System.out.println("Joe: \n" +
                "     Age: " + Joe.getAge() + "\n" +
                "     Breed: " + Joe.getBreed() + "\n" +
                "     Joe has: " + Joe.getNumOfFeet() + " paws \n" +
                "     Joe is awake: " + Joe.isAwake() + "\n"
        );
        Bob.setAge(7);
        Bob.setGender(false);
        Bob.setEyeCount(1);
        System.out.println("Joe: \n" +
                        "     Age: " + Bob.getAge() + "\n" +
                        "     Breed: " + Bob.getBreed() + "\n" +
                        "     Bob has: " + Bob.getNumOfFeet() + " paws \n" +
                        "     Bob is awake: " + Bob.isAwake() + "\n"
        );
    }





}
