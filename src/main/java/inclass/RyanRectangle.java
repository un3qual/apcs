package inclass;
/**
 * Ryan's rectangle class
 *
 * @author Ryan Ramsdell
 */
public class RyanRectangle
{

    private double width = 1;
    private double height = 1;

    /**
     * No-arg constructor for objects of class RyanRectangle
     */
    public RyanRectangle()
    {

    }
    /**
     * Constructor for objects of class RyanRectangle
     */
    public RyanRectangle(double _width, double _height) {
        this.width = _width;
        this.height = _height;
    }
    /**
     * Get area
     * @return area
     */
    public double getArea() {
        return this.width * this.height;
    }

    /**
     * Get perimeter
     * @return perimeter
     */
    public double getPerimeter() {
        return (2 * this.width) + (2 * this.height);
    }

    /**
     * Get width
     * @return width
     */
    public double getWidth() {
        return width;
    }

    /**
     * Set width
     *
     * @param  _width The width
     */
    public void setWidth(double _width) {
        this.width = _width;
    }

    /**
     * Get height
     *
     * @return height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Set height
     * @param _height The height
     */
    public void setHeight(double _height) {
        this.height = _height;
    }
}
