package inclass;


import java.util.Scanner;


public class PasswordValidator {

    public static boolean isValid(String p) {
        boolean[] validations = new boolean[2];
        int numCount = 0;
        int specialCount = 0;

        if(p.length() >= 8){
            validations[0] = true;
        }
        for (int i = 0; i <  p.length() - 1; i++) {
            if (String.valueOf(p.charAt(i)).matches("[\\da-zA-Z@#$%&*\\-_+*]")) {
                if(String.valueOf(p.charAt(i)).matches("[@#$%&*\\-_+*]")) {
                    specialCount++;
                } else if (Character.isDigit(p.charAt(i))) {
                    numCount++;
                }
            } else {
                return false;
            }
        }

        return (numCount >= 2 && specialCount >= 1 && validations[0]);


    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a password to validate: ");
        String pw = input.next();

        System.out.println(input.next() + " is valid? " + String.valueOf(isValid(pw)));


    }



}
