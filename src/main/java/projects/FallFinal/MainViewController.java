package projects.FallFinal;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.*;
import java.util.*;


/**
 * Main class for ATM
 * @author Ryan Ramsdell
 */

public class MainViewController extends Application {

    // Formatter for nice looking account balance
    static NumberFormat formatter = new DecimalFormat("###.##");
    // Create variable for the account balance
    static double _cachedBal = 0;

    /*
     * Begin standard required JavaFX methods
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
    /*
     * End JavaFX methods
     */

    /**
     * Calculate the account balance and change the class variable
     * @param userData the userData to change as a JSONObject
     */
    public static void calcBal(JSONObject userData) {
        _cachedBal = Double.parseDouble(String.valueOf(userData.get("cached_acct_balance")));
    }

    /**
     * Change the value in the data store file based on params
     * @param userData The userData to change
     * @param obj The entire data store
     * @param value Amount to change by
     * @param type Withdraw or Deposit
     * @return The newly modified userData
     */
    public static JSONObject addAcountAction(JSONObject userData, JSONObject obj, String value, boolean type) {
        // true = deposit, false = withdraw
        if(type == true) {
            // Set "cached_acct_balance" to the current value + the deposited value and format it with formatter
            userData.put("cached_acct_balance", formatter.format(userData.getDouble("cached_acct_balance") + Double.parseDouble(value)));
        } else {
            userData.put("cached_acct_balance", formatter.format(userData.getDouble("cached_acct_balance") - Double.parseDouble(value)));
        }

        // Recalculate the new balance
        calcBal(userData);
        // Edit the store file
        updateData(obj);
        return userData;
    }

    /**
     * Edit the data store file with the new data
     * @param obj the data to insert
     */
    public static void updateData(JSONObject obj) {
        try {
            File file = new File("users.json");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(String.valueOf(obj));
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when login is successful
     * @param scene the scene to place the ui inside
     * @param stage stage for the scene
     *              <em>NOTE: This is optional and only for reusability</em>
     * @param userData the userdata to use for the ui
     */
    public static void doATM(Scene scene, Stage stage, JSONObject userData, JSONObject obj) {
        /**
         * Delare UI stuff
         */
        // Deposit button
        Button DepBtn = (Button) scene.lookup("#DepBtn");
        // Withdraw button
        Button WdrBtn = (Button) scene.lookup("#WdrBtn");
        // Logout button
        Button LogoutBtn = (Button) scene.lookup("#LogoutBtn");
        // Account Manager button
        Button ActMgrBtn = (Button) scene.lookup("#ActMgrBtn");
        // Save Edits Button
        Button SaveBtn = (Button) scene.lookup("#SaveBtn");
        //  Enable editing button
        Button EditBtn = (Button) scene.lookup("#EditBtn");
        /**
         * This is a hacky way of detecting if the user is in edit mode because I can't change a variable inside the event handlers
         * Any variables accessed inside the EventHandler need to be final or effectively final
         */
        EditBtn.setText("Enable Editing");

        // Account balance label
        Label ActBal = (Label) scene.lookup("#ActBal");
        // Greeting label
        Label GreetLbl = (Label) scene.lookup("#GreetLbl");

        // Deposit TextField
        TextField ActDepTF = (TextField) scene.lookup("#ActDepTF");
        // Withdraw TextField
        TextField ActWdrTF = (TextField) scene.lookup("#ActWrdTF");
        // Name TextField
        TextField NameTF = (TextField) scene.lookup("#NameTF");
        // Address TextField
        TextField AddressTF = (TextField) scene.lookup("#AddressTF");
        // Phone TextField
        TextField PhoneTF = (TextField) scene.lookup("#PhoneTF");
        // Email TextField
        TextField EmailTF = (TextField) scene.lookup("#EmailTF");

        /**
         * Setup cachedActBal from the account action history
         * Doing it twice is a hacky way to ensure that the data is not being access from the cache in case the user changes the data store manually
         */
        calcBal(userData);
        calcBal(userData);


        /**
         * Add userData to UI
         */
        /**
         * Replace all text in labels with the userData for that user
         */
        GreetLbl.setText(GreetLbl.getText().replace("${FIRST_NAME}", String.valueOf(userData.get("name"))));
        ActBal.setText(ActBal.getText().replace("${ACT_BAL}", String.valueOf(userData.get("cached_acct_balance"))));

        /**
         * Fill the TextFields with the userData and disable them to stop accidental editing
         */
        NameTF.setText(String.valueOf(userData.get("name")));
        NameTF.setDisable(true);
        PhoneTF.setText(String.valueOf(userData.get("phone")));
        PhoneTF.setDisable(true);
        AddressTF.setText(String.valueOf(userData.get("address")));
        AddressTF.setDisable(true);
        EmailTF.setText(String.valueOf(userData.get("email")));
        EmailTF.setDisable(true);

        /**
         * Disable the save button
         */
        SaveBtn.setDisable(true);


        /**
         * Handles a click on the Edit button
         */
        EditBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                // The hacky state tracking
                if (EditBtn.getText().equalsIgnoreCase("Enable Editing")) {

                    // Enable all text fields for editing
                    NameTF.setDisable(false);
                    PhoneTF.setDisable(false);
                    AddressTF.setDisable(false);
                    EmailTF.setDisable(false);
                    // Set button text to "Cancel Edits"
                    EditBtn.setText("Cancel Edits");
                    SaveBtn.setDisable(false);


                } else {
                    // The hacky state tracking
                    EditBtn.setText("Enable Editing");
                    // Disable all fields and reset them (if the user cancels the edits_
                    NameTF.setText(String.valueOf(userData.get("name")));
                    NameTF.setDisable(true);
                    PhoneTF.setText(String.valueOf(userData.get("phone")));
                    PhoneTF.setDisable(true);
                    AddressTF.setText(String.valueOf(userData.get("address")));
                    AddressTF.setDisable(true);
                    EmailTF.setText(String.valueOf(userData.get("email")));
                    EmailTF.setDisable(true);
                    SaveBtn.setDisable(true);

                }


            }
        });
        SaveBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Only allow saving if the user is in edit mode
                if(EditBtn.getText().equalsIgnoreCase("Cancel Edits")){

                    // Hacky state tracking
                    EditBtn.setText("Enable Editing");

                    //Save the data in the cache
                    userData.put("name", String.valueOf(NameTF.getText()));
                    userData.put("phone", String.valueOf(PhoneTF.getText()));
                    userData.put("address", String.valueOf(AddressTF.getText()));
                    userData.put("email", String.valueOf(EmailTF.getText()));

                    // Set the contents of the text fields to the new userData
                    NameTF.setText(String.valueOf(userData.get("name")));
                    PhoneTF.setText(String.valueOf(userData.get("phone")));
                    AddressTF.setText(String.valueOf(userData.get("address")));
                    EmailTF.setText(String.valueOf(userData.get("email")));

                    // Disable all TextFields after saving
                    NameTF.setDisable(true);
                    PhoneTF.setDisable(true);
                    AddressTF.setDisable(true);
                    EmailTF.setDisable(true);
                    SaveBtn.setDisable(true);

                    // Save data to the file
                    updateData(obj);


                }

            }
        });

        /**
         * Handles a new deposit
         */
        DepBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Change the data
                addAcountAction(userData, obj, String.valueOf(ActDepTF.getText()), true);

                // Empty the text field
                ActDepTF.setText("");

                // Update the cache
                calcBal(userData);
                // Change the label
                ActBal.setText(String.valueOf("Account Balance: " + formatter.format(_cachedBal)));
            }
        });
        /**
         * Handles a withdraw
         */
        WdrBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Change the data
                addAcountAction(userData, obj, String.valueOf(ActWdrTF.getText()), false);

                // Empty the text field
                ActWdrTF.setText("");

                // Update the cache
                calcBal(userData);

                // Change the label
                ActBal.setText(String.valueOf("Account Balance: " + formatter.format(_cachedBal)));
            }
        });
        /**
         * Handles a logout
         */
        LogoutBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Close the app
                Platform.exit();
                System.exit(0);
            }
        });


    }


}
