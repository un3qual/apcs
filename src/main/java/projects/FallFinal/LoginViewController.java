package projects.FallFinal;


import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import org.json.*;

public class LoginViewController extends Application {

    // list of allowed keys in the PIN TextField
    final KeyCode[] allowedNums = {KeyCode.DIGIT0, KeyCode.DIGIT1, KeyCode.DIGIT2, KeyCode.DIGIT3, KeyCode.DIGIT4, KeyCode.DIGIT5, KeyCode.DIGIT6, KeyCode.DIGIT7, KeyCode.DIGIT8, KeyCode.DIGIT9};
    // Vars for begining the session
    boolean shouldInitLogin = false;
    boolean shouldDoSession = false;

    /**
     * Required JavaFX method
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Reset the login TextField
     * @param PinTF
     */
    public static void resetLoginField(TextField PinTF) {
        // Delet the contents
        PinTF.setText("");
        // Enable the text field
        PinTF.setDisable(false);
        // Show it
        PinTF.setVisible(true);
    }

    /**
     * Required JavaFX method
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        // Get the view
        Parent root = FXMLLoader.load(getClass().getResource("Views/LoginView.fxml"));
        // Set window size to 600x400
        Scene scene = new Scene(root, 600, 400);

        // Set window title
        primaryStage.setTitle("Login");
        // Set the scene for the stage
        primaryStage.setScene(scene);
        // Show the stage
        primaryStage.show();

        // TextField for the PIN
        TextField PinTF = (TextField)scene.lookup("#PinTF");
        // The progress indicator
        ProgressIndicator LoginProgInd = (ProgressIndicator)scene.lookup("#loginProgIndicator");
        // Hide the progress indicator
        LoginProgInd.setVisible(false);
        // Show the text field
        PinTF.setVisible(true);
        // Label to tell the user to login
        Label loginLbl = (Label)scene.lookup("#loginLbl");
        // Set the text of the label
        loginLbl.setText("Login with your PIN");
        // Set the color of the label
        loginLbl.setTextFill(Color.web("#323232"));
        // Center the label
        loginLbl.setLayoutX(240);

        /**
         * Called when a key is pressedin the PIN text field
         */
        PinTF.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                // If the key entered is not a number or the login sequence has started
                if (!Arrays.asList(allowedNums).contains(ke.getCode()) || shouldInitLogin) {
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    // Delete the last character entered
                    PinTF.setText(PinTF.getText().replaceAll("[^\\d.]", ""));
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                } else {
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    // Delete the last character entered
                    PinTF.setText(PinTF.getText().replaceAll("[^\\d.]", ""));
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    // If the required PIN length has been entered
                    if (PinTF.getText().length() > 3) {
                        // Start logging in
                        shouldInitLogin = true;
                        // Disable the TextField
                        PinTF.setDisable(true);
                    }
                }



            }
        });
        /**
         * Called when a key is released in the TextField
         */
        PinTF.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                // If the key entered is not a number or the login sequence has started
                if (!Arrays.asList(allowedNums).contains(ke.getCode()) || shouldInitLogin) {
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    // Delete the last character entered
                    PinTF.setText(PinTF.getText().replaceAll("[^\\d.]", ""));
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                } else {
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    // Delete the last character entered
                    PinTF.setText(PinTF.getText().replaceAll("[^\\d.]", ""));
                    // Put the cursor at the end of the TextField
                    PinTF.positionCaret(PinTF.getText().length());
                    if (PinTF.getText().length() > 3) {
                        //System.out.println("Done :D");
                        shouldInitLogin = true;
                        PinTF.setDisable(true);
                    }
                }
                // If login should start
                if (shouldInitLogin) {

                    try {
                        // Disable the TextField
                        PinTF.setDisable(true);
                        // Set the PIN to the first 4 characters of the TextField
                        String PIN = PinTF.getText().substring(0, 4);
                        // Hide the TextField
                        PinTF.setVisible(false);
                        // Show the loading indicator
                        LoginProgInd.setVisible(true);

                        // Open the data file
                        String data = new Scanner(new File("users.json")).useDelimiter("\\A").next();

                        // Make a JSONObject with the data
                        JSONObject obj = new JSONObject(data);
                        // Get only the Users
                        JSONArray allData = obj.getJSONArray("Users");

                        // Loop through all of the users
                        for (int i = 0, size = allData.length(); i < size; i++) {
                            // Get only the user with the correct PIN
                            JSONObject userData = allData.getJSONObject(i);
                            // If the PIN entered equals the PIN of the current User in the loop
                            if (userData.getString("pin").equalsIgnoreCase(PIN)) {
                                // We should start the session
                                shouldDoSession = true;
                                // For debuging. Tell the console who we are logging in ias
                                System.out.println("Logging in as " + userData.get("name"));
                                // Change the label text
                                loginLbl.setText("Logging in");
                                loginLbl.setTextFill(Color.web("#323232"));
                                loginLbl.setLayoutX(267);
                                // Hide the stage
                                primaryStage.hide();

                                // Change the layout of the scene to the main view
                                scene.setRoot(FXMLLoader.load(getClass().getResource("Views/MainView.fxml")));
                                // New stage
                                Stage MainStage = new Stage();
                                // Change the title
                                MainStage.setTitle("ATM");
                                MainStage.setScene(scene);
                                MainStage.show();
                                // Do the ATM stuff in the MainView class
                                MainViewController.doATM(scene, MainStage, userData, obj);

                                // Stop looking for users
                                break;

                            } else {
                                // Don't do the session
                                shouldDoSession = false;
                                // Change label text
                                loginLbl.setText("Incorrect PIN");
                                // Enable the text field to enter a new PIN
                                PinTF.setDisable(false);
                                // Reset the TextField
                                resetLoginField(PinTF);
                                // Hide the loading spinner
                                LoginProgInd.setVisible(false);
                                // We should not start the login
                                shouldInitLogin = false;
                                // Reset the label
                                loginLbl.setTextFill(Color.web("#FF0000"));
                                loginLbl.setLayoutX(259);
                                // Enable the text field
                                PinTF.setDisable(false);

                            }

                        }


                    } catch (FileNotFoundException e) {
                        /**
                         * Handle not finding the file
                         */
                        e.printStackTrace();
                    } catch (IOException e) {
                        /**
                         * More exception handling
                         */
                        e.printStackTrace();
                    }

                }

            }
        });

    }




}
