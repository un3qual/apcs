package hw.L5;

import java.util.*;

/**
 * The class for PE:7.5
 * @author Ryan Ramsdell
 */
public class E5_PrintUniqueNumbers {

    static List<Integer> nums = new ArrayList<Integer>();
    static Set<Integer> tempUniqueSet = new HashSet<>();

    /**
     * Prints number of unique numbers and each unique number
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter some ints with 0 as the last int: ");

        int lastNum;
        // Loop through each number in the commmand line
        while(in.hasNextInt()) {
            lastNum = in.nextInt();
            // Add the number to the array
            nums.add(lastNum);
            if (lastNum == 0)
                break;

        }
        // Sort the array from least to greatest
        Collections.sort(nums);
        // Add all nums to the Set
        tempUniqueSet.addAll(nums);

        // Clear the List
        nums.clear();
        // Add all from the Set to the List
        nums.addAll(tempUniqueSet);

        // Sort the array from least to greatest
        Collections.sort(nums);

        System.out.format("There are %d unique numbers. They are: ", nums.size());
        for (int theNum : nums) {
            System.out.print(nums.get(theNum) + " ");
        }


    }

}
