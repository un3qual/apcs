package hw.L5;

import java.util.Scanner;

/**
 * The class for PE:8.5
 * @author Ryan Ramsdell
 */
public class E8_5_AlgebraAddMatrix {


    /**
     * Print matrix with nice formatting
     * @param grid
     */
    static void printMatrix(double[][] grid) {
        for(int r=0; r<grid.length; r++) {
            for(int c=0; c<grid[r].length; c++)
                System.out.print(grid[r][c] + " ");
            System.out.println("");
        }
    }

    /**
     * Add 2 matrixes
     * @param a
     * @param b
     * @return a + b
     */
    public static double[][] addMatrix(double[][] a, double[][] b) {
        double[][] c = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter 3x3 matrix 1 with a string to denote the end: ");
        double a[][] = new double[3][3];
        double b[][] = { {0,2,4}, {1, 4.5, 2.2}, {1.1, 4.3, 5.2} };
        double c[][] = new double[3][3];


        int itrtrI = 0;
        int itrtrJ = 0;
        while(in.hasNextDouble()) {
          //System.out.println("\033[0;1m 1 \033[0;0m");
//            System.out.println(itrtrI + ", " + itrtrJ);
            a[itrtrI][itrtrJ] = in.nextInt();
            itrtrJ++;

            if((itrtrJ) % 3 == 0) {
                itrtrI++;
                itrtrJ = 0;
               // System.out.println("\033[0;1m" + itrtrI + ", " + itrtrJ + "\033[0;0m");
            }

        }
        itrtrI = 0;
        itrtrJ = 0;
        //printMatrix(a);
        in.close();


       // printMatrix(b);

        c = addMatrix(a, b);
        printMatrix(c);


    }


}
