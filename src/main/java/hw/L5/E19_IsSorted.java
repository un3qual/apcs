package hw.L5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * The class for PE:7.19
 * @author Ryan Ramsdell
 */
public class E19_IsSorted {

    /**
     * Returns true if an array is sorted from least to greatest
     * @param list
     * @return
     */
    public static boolean isSorted(int[] list) {
        // Init a list that will be sorted to be compared to
        int[] sortedList = new int[list.length];
        System.arraycopy(list, 0, sortedList, 0, list.length);
        Arrays.sort(sortedList);

        return (Arrays.equals(sortedList, list));
    }

    /**
     * Sample test class for isSorted
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter some ints with a string at the end: ");
        List<Integer> numsList = new ArrayList<Integer>();

        int lastNum;
        // Loop through each number in the commmand line
        while(in.hasNextInt()) {
            lastNum = in.nextInt();
            // Add the number to the array
            numsList.add(lastNum);


        }
        // Create the array of numbers
        int[] nums = new int[numsList.size()];
        // "Move" the numbers from the ArrayList to the Array
        for (int i = 0; i < numsList.size(); i++) {
            nums[i] = numsList.get(i);
        }
        if (isSorted(nums)) {
            System.out.println("The array is sorted");
        } else {
            System.out.println("The array is not sorted");
        }


    }

}
