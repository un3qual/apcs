package hw.L5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * The class for PE:8.19
 * @author Ryan Ramsdell
 */
public class E8_19_Pattern4Consec {


    public static boolean isConsecutiveFour(int[][] v) {
        for(int i = 0; i < 10; i++) {
            for (int j = 0; j < v.length; j++) {
                for (int k = 0; k < v[0].length; k++) {
                    if(v[j][k] == v[j][k+1] && v[j][k+1] == v[j][k+2] && v[j][k+2] == v[j][k+3]) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> nums = new ArrayList<ArrayList<Integer>>();
        Scanner in = new Scanner(System.in);
        while(in.hasNextLine()) {
            
        }
        System.out.println("-------\n" + in.nextLine());
    }
}
