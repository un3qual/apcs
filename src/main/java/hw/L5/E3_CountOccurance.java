package hw.L5;

import hw.xc.Quiz3.InterestCalc;

import java.util.*;

/**
 * The class for PE:7.3
 * @author Ryan Ramsdell
 */

public class E3_CountOccurance {

    // Create a list to put the numbers in to
    static List<Integer> nums = new ArrayList<Integer>();

    /**
     * Prints how many times each number occurs from input
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter some ints: ");

        int lastNum;
        // Loop through each number in the commmand line
        while(in.hasNextInt()) {
            lastNum = in.nextInt();
            // Add the number to the array
            nums.add(lastNum);
            if (lastNum == 0)
                break;
            
        }
        // Sort the array from least to greastest
        Collections.sort(nums);


        // Index
        int itrtr = 0;
        // Count of occurances
        int count = 1;
        // Loop through each number
        for(int theNum : nums) {

            itrtr++;
            if(itrtr > 0) {
                // Print how many times each number occurs
                if (itrtr == nums.size()) {
                    System.out.format("%d occurs %d times\n", theNum, count);
                } else if(nums.get(itrtr) == nums.get(itrtr - 1)) {
                    count++;
                } else {
                    if (count > 1) {
                        System.out.format("%d occurs %d times\n", theNum, count);
                        count = 1;
                    }
                    System.out.format("%d occurs %d time\n", theNum, count);
                    count = 1;
                }
            }

        }


    }
}

