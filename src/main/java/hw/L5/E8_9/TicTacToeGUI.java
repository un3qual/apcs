package hw.L5.E8_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * The class for PE:8.9
 * @author Ryan Ramsdell
 */
public class TicTacToeGUI extends JFrame {
    // Init all elements
    private JPanel rootPanel;
    private JLabel turnLabel;
    private JButton btn00;
    private JButton btn20;
    private JButton btn10;
    private JButton btn11;
    private JButton btn12;
    private JButton btn02;
    private JButton btn01;
    private JButton btn22;
    private JButton btn21;
    Random rand = new Random();
    private boolean currentTurn;
    private int[][] board = new int[3][3];

    /**
     * Constructor
     */
    public TicTacToeGUI(){
        super("TicTacToeGUI");
        setContentPane(rootPanel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        /**
         * Setup
         */
        btn20.setEnabled(true);
        btn20.setText("");
        btn10.setEnabled(true);
        btn10.setText("");
        btn00.setEnabled(true);
        btn00.setText("");
        btn21.setEnabled(true);
        btn21.setText("");
        btn11.setEnabled(true);
        btn11.setText("");
        btn01.setEnabled(true);
        btn01.setText("");
        btn22.setEnabled(true);
        btn22.setText("");
        btn12.setEnabled(true);
        btn12.setText("");
        btn02.setEnabled(true);
        btn02.setText("");
        editTurnLabel(rand.nextBoolean());

        btn00.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn00, 0,0);

            }
        });
        btn01.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn01, 0,1);

            }
        });
        btn02.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn02, 0,2);

            }
        });
        btn10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn10, 1,0);

            }
        });
        btn11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn11, 1,1);

            }
        });
        btn12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn12, 1,2);

            }
        });
        btn20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn20, 2,0);

            }
        });
        btn21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn21, 2,1);

            }
        });
        btn22.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doButtonAction(btn22, 2,2);

            }
        });




        setVisible(true);
    }
    private void editTurnLabel(boolean b) {
        if(b) {
            turnLabel.setText("Turn: O");
            currentTurn = true;
        } else {
            turnLabel.setText("Turn: X");
            currentTurn = false;
        }
    }
    private void doButtonAction(JButton b, int posX, int posY) {
        b.setEnabled(false);
        if(currentTurn) {
            b.setText("O");
        } else {
            b.setText("X");
        }


        board[posX][posY] = (currentTurn) ? 1 : 2;
        hasWon(currentTurn);
        currentTurn = !currentTurn;
        editTurnLabel(currentTurn);

    }
    private void showWinMessage(int w) {
        if (w == 1) {
            JOptionPane.showMessageDialog(new JFrame(), "O has won!", "Winner",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "X has won!", "Winner",
                    JOptionPane.INFORMATION_MESSAGE);
        }
        System.exit(0);

    }
    private void hasWon(boolean p) {
        // Horizontal
        int pl = (p) ? 1 : 2;
        for (int i = 0; i < board.length; i++) {
            if((board[i][0] == board[i][1] && board[i][1] == board[i][2]) ){
                if(board[i][0] > 0) {
                    showWinMessage(board[i][0]);
                }
            }
        }
        // Vertical
        for(int i = 0; i < board.length; i++) {
            if((board[0][i] == board[1][i] && board[1][i] == board[2][i]) ){
                if(board[0][i] > 0) {
                    showWinMessage(board[0][i]);
                }
            }
        }
        // L-R Diagonal
        if(board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            if(board[0][0] > 0) {
                showWinMessage(board[0][0]);
            }
        }
        // R-L Diagonal
        if(board[2][0] == board[1][1] && board[1][1] == board[0][2]) {
            if(board[2][0] > 0) {
                showWinMessage(board[2][0]);
            }

        } else {
            boolean tie = true;
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    if (board[i][j] == 0) {
                        tie = false;
                    }
                }

            }
            if (tie) {
                JOptionPane.showMessageDialog(new JFrame(), "It's a tie!!", "Tue",
                        JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
        }




    }



}
