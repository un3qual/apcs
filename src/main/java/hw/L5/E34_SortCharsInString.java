package hw.L5;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The class for PE:7.34
 * @author Ryan Ramsdell
 */
public class E34_SortCharsInString {

    /**
     * Sorts a string alphabetically by character
     * @param s
     * @return
     */
    public static String sort(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        return String.valueOf(chars);

    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String theString = in.next();
        System.out.format("%s sorted is %s", theString, sort(theString));
    }
}
