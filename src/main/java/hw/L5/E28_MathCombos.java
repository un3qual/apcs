package hw.L5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The class for PE:7.28
 * @author Ryan Ramsdell
 */
public class E28_MathCombos {

    /**
     * Tells the user all combonations of picking 2 numbers from a list of numbers
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter some ints with a string at the end: ");

        List<Integer> numsList = new ArrayList<Integer>();

        int lastNum;
        // Loop through each number in the commmand line
        while(in.hasNextInt()) {
            lastNum = in.nextInt();
            // Add the number to the array
            numsList.add(lastNum);
        }
        System.out.print("All combos of picking two numbers from the numbers are: ");
        // Loop through the list of nums
        for (int i = 0; i < numsList.size(); i++) {
            for (int j = 0; j < numsList.size(); j++) {
                // Print the combination
                System.out.format("[%d : %d], ", numsList.get(i), numsList.get(j));

            }
        }
    }

}
