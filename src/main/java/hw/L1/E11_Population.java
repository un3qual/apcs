package hw.L1;

/**
 * The class for PE1.11
 * @author Ryan Ramsdell
 */
public class E11_Population {

    /**
     * Prints estimated population for next 5 years based on data from now
     * @param args
     */
    public static void main(String[] args){
        // 1 ___ every ___ seconds
        float birthEvery = 7.0f;
        float deathEvery = 13.0f;
        float immigrantEvery = 45.0f;
        // 312,032,486
        float currentPop = 312032486.0f;
        float daysPerYear = 365.0f;
        float hrsPerDay = 24.0f;
        float minPerHr = 60.0f;
        float secPerMin = 60.0f;
        float secPerYear = secPerMin * minPerHr * hrsPerDay * daysPerYear;
        float birthsPerYear = secPerYear / birthEvery;
        float deathsPerYear = secPerYear / deathEvery;
        float immigrantsPerYear = secPerYear / immigrantEvery;
        float netPerYear = birthsPerYear - deathsPerYear + immigrantsPerYear;


        System.out.println("Now: " + currentPop);
        for (float i = 1.0f; i < 6.0f; i++) {
            float popThisYear = currentPop + (netPerYear * i);
            System.out.println("Year " + i + ": " + popThisYear);
        }

    }
}
