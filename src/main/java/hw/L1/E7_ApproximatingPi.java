package hw.L1;

/**
 * The class for PE:1.7
 * @author Ryan Ramsdell
 */
public class E7_ApproximatingPi {

    /**
     * Displays the result of an approximation of Pi
     * @param args
     */
    public static void main(String[] args){
       System.out.println(4 * (1 - (1/3) + (1/5) - (1/7) + (1/9) - (1/11) + (1/13)));
    }
}
