package hw.L1;

/**
 * The class for PE:1.6
 * @author Ryan Ramsdell
 */
public class E6_SummationOfASeries {
    /**
     * Displays the result of a summation of a series
     * @param args
     */
    public static void main(String[] args){
        /**
         * The final result of the summation
         */
        int result = 0;

        for (int i = 1; i < 10; i++) {
            result += i;
        }
        System.out.println(result);
    }
}
