package hw.L1;

/**
 * The class for PE:1.4
 * @author Ryan Ramsdell
 */
public class E4_PrintingATable {

    /**
     * Prints a table of ints
     * @param args
     */
    public static void main(String[] args){
        System.out.println("a    a^2  a^3\n" +
                "1    1    1\n" +
                "2    4    8\n" +
                "3    9    27\n" +
                "4    16   64");
    }
}
