package hw.L1;

/**
 * The Welcome1 class for PE:1.1
 * @author Ryan Ramsdell
 */
public class E1_DisplayingThreeMessages {

    /**
     * Prints some text.
     * @param args
     */
    public static void main(String[] args){
        System.out.println("Welcome to Java\n" +
                "Welcome to Computer Science\n" +
                "Programming is fun!");
    }
}
