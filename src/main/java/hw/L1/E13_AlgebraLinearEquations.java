package hw.L1;

/**
 * The class for PE1.13
 * @author Ryan Ramsdell
 */
public class E13_AlgebraLinearEquations {

    public static void main(String[] args){
        float a = 3.4f;
        float b = 50.2f;
        float c = 2.1f;
        float d = .55f;
        float e = 44.5f;
        float f = 5.9f;

        float x = ((e * d) - (b * f)) / ((a * d) - (b * c));
        float y = ((a * f) - (e * c)) / ((a * d) - (b * c));

        System.out.println("For the system of linear equations: \n 3.4x + 50.2y = 44.5 \n 2.1x + .55y = 5.9 \n\n" +
        "x = " + x + "\n" +
        "y = " + y + "\n");
    }

}
