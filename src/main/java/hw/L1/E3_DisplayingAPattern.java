package hw.L1;

/**
 * The class for PE:1.3
 * @author Ryan Ramsdell
 */
public class E3_DisplayingAPattern {

    /**
     * Prints "Java" as ASCII art
     * @param args
     */
    public static void main(String[] args){
        System.out.println("     J     A     V     V    A      \n" +
        "     J    A A     V   V    A A     \n" +
        " J   J   AAAAA     V V    AAAAA    \n" +
        "  J J   A     A     V    A     A   \n");

    }
}
