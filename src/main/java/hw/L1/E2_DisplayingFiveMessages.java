package hw.L1;

/**
 * The Welcome1 class for PE:1.2
 * @author Ryan Ramsdell
 */
public class E2_DisplayingFiveMessages {

    /**
     * Prints "Welcome to Java" 5 times
     * @param args
     */
    public static void main(String[] args){
        for(int i=1; i < 5; i++){
            System.out.println("Welcome to Java");
        }
    }
}