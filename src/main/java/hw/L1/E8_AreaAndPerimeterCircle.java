package hw.L1;

/**
 * The class for PE1.8
 * @author Ryan Ramsdell
 */

public class E8_AreaAndPerimeterCircle {


    /**
     * Prints area area & perimeter of a circle with radius 5.5
     * @param args
     */
    public static void main(String[] args){
        double radius = 5.5;
        System.out.println("Area: " + calcArea(radius));
        System.out.println("Perimeter: " + calcPerimeter(radius));
    }

    /**
     * Calculates area of circle
     * @param radius
     * @return Area
     */
    public static double calcArea(double radius){
        return Math.PI * Math.pow(radius, 2);
    }

    /**
     * Calculates perimeter of circle
     * @param radius
     * @return
     */
    public static double calcPerimeter(double radius){
        return 2 * radius * Math.PI;
    }
}
