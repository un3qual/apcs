package hw.L1;

/**
 * The class for PE1.12
 * @author Ryan Ramsdell
 */
public class E12_AvgSpeedKmh {

    /**
     * Prints km/h for a runner
     * @param args
     */
    public static void main(String[] args){
        float distance = 24.0f;
        float mileTimeHours = 1.0f;
        float mileTimeMinutes = 40.0f;
        float mileTimeSeconds = 35.0f;
        float mileTimeTotalSeconds = (mileTimeHours * 3600.0f) + (mileTimeMinutes * 60.0f) + mileTimeSeconds;
        float mileToKmh = 1.6f;
        float milesPerSecond = distance / mileTimeTotalSeconds;
        float kmPerSecond = milesPerSecond * mileToKmh;
        float kmPerHour = kmPerSecond * 3600;

        System.out.println("Speed: " + kmPerHour);
    }

}
