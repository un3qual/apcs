package hw.L1;

/**
 * The class for PE:1.5
 * @author Ryan Ramsdell
 */
public class E5_ComputingExpressions {

    /**
     * Computes the answer to an expression and prints it
     * @param args
     */
    public static void main(String[] args){
        System.out.println((9.5 * 4.5 - 2.5 * 3)/(45.5 - 3.5));
    }
}
