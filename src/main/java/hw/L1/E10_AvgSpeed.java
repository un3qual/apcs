package hw.L1;

/**
 * The class for PE1.10
 * @author Ryan Ramsdell
 */
public class E10_AvgSpeed {


    /**
     * Prints average speed in miles per hour
     * @param args
     */
    public static void main(String[] args){
        float kilometers_ran = 14.0f;
        float minutes = 45.0f;
        float seconds = 30.0f;
        float totalSeconds = (minutes * 60) + seconds;
        float averageSpeedKmh = averageSpeedInKmh(totalSeconds, kilometers_ran);
        float avgInMph = kmhToMph(averageSpeedKmh);

        System.out.println("Avg Speed: " + avgInMph + "mph");


    }

    /**
     * Returns average speed in km/h
     * @param seconds
     * @param km
     * @return Avg speed in km/h
     */
    public static float averageSpeedInKmh(float seconds, float km){
        float hours = seconds / 3600.0f;

        return km / hours;
    }

    /**
     * Converts kmh to mph
     * @param kmh
     * @return miles per hour
     */
    public static float kmhToMph(float kmh){
        float kmPerMile = 1.6f;
        return kmh / kmPerMile;
    }

}
