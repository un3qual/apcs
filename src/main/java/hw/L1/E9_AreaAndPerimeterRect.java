package hw.L1;

/**
 * The class for PE1.9
 * @author Ryan Ramsdell
 */
public class E9_AreaAndPerimeterRect {

    /**
     * Prints area area & perimeter of a rectangle with width: 4.5, height: 7.9
     * @param args
     */
    public static void main(String[] args){
        float width = 4.5f;
        float height = 7.9f;
        System.out.println("Area: " + calcArea(width, height));
        System.out.println("Perimeter: " + calcPerimeter(width, height));
    }

    /**
     * Calculates area of rectangle
     * @param width
     * @param height
     * @return Area
     */
    public static float calcArea(float width, float height){
        return width * height;
    }

    /**
     * Calculates perimeter of rectangle
     * @param width
     * @param height
     * @return
     */
    public static float calcPerimeter(float width, float height){
        return (2 * width) + (2 * height);
    }
}
