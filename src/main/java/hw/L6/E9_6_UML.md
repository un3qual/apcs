|     Stopwatch     |
|:-----------------:|
| startTime endTime |
| endTime           |
| ----------------- |
| Stopwatch()       |
| get startTime     |
| get stopTime      |
| start()           |
| stop()            |
| getElapsedTime()  |