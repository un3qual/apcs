package hw.L6.E9_10;

/**
 * The class for PE:9.10
 * @author Ryan Ramsdell
 */
public class QuadraticEquation {

    private double a;
    private double b;
    private double c;

    /**
     * Constructor
     * @param _a
     * @param _b
     * @param _c
     */
    public QuadraticEquation(double _a, double _b, double _c) {
        this.a = _a;
        this.b = _b;
        this.c = _c;

    }

    /**
     * @return a
     */
    public double getA() {
        return a;
    }

    /**
     * @return b
     */
    public double getB() {
        return b;
    }

    /**
     * @return c
     */
    public double getC() {
        return c;
    }

    /**
     * @return the discriminant
     */
    public double getDiscriminant() {
        return (Math.pow(this.b, 2.0)) - (4 * this.a * this.c);
    }

    /**
     * @return root 1
     */
    public double getRoot1() {
        if(this.getDiscriminant() >= 0) {
            return ((this.b * -1) + (Math.sqrt(this.getDiscriminant()))) / (this.a * 2);
        } else {
            return 0;
        }
    }

    /**
     * @return root 2
     */
    public double getRoot2() {
        if(this.getDiscriminant() >= 0) {
            return ((this.b * -1) - (Math.sqrt(this.getDiscriminant()))) / (this.a * 2);
        } else {
            return 0;
        }
    }

}
