package hw.L6.E9_10;

import java.util.Scanner;

/**
 * The test class for PE:9.10
 * @author Ryan Ramsdell
 */
public class TestQuadratic {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter values for a, b, and c: ");
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
        QuadraticEquation q = new QuadraticEquation(a, b, c);
        if(q.getDiscriminant() == 0) {
            System.out.println("Root: " + q.getRoot1());
        } else {
            System.out.println("Discriminant: " + q.getDiscriminant());
            System.out.println("Root1: " + q.getRoot1());
            System.out.println("Root2: " + q.getRoot2());
        }
    }

}
