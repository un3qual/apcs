|   QuadraticEquation   |
|:---------------------:|
| a                     |
| b                     |
| c                     |
| --------------------- |
| QuadraticEquation()   |
| getA()                |
| setA()                |
| getB()                |
| setB()                |
| getC()                |
| setC()                |
| getRoot1()            |
| getRoot2()            |
| getDiscriminant()     |