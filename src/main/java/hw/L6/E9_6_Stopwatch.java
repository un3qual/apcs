package hw.L6;

import java.util.Scanner;

/**
 * The class for PE:9.6
 * @author Ryan Ramsdell
 */
public class E9_6_Stopwatch {

    private double startTime = System.currentTimeMillis();
    private double stopTime;
    private boolean isStarted = false;


    public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean started) {
        isStarted = started;
    }


    /**
     * Constructor
     */
    public E9_6_Stopwatch() {
        this.startTime = System.currentTimeMillis();
        setStarted(false);
    }

    /**
     * Start the timer
     */
    public void start(){
        this.startTime = System.currentTimeMillis();
        setStarted(true);
    }

    /**
     * Stop the timer
     */
    public void stop() {
        this.stopTime = System.currentTimeMillis();
        setStarted(false);
    }

    /**
     * @return How much time has elapsed
     */
    public double getElapsedTime() {
        return this.stopTime - this.startTime;
    }

}

