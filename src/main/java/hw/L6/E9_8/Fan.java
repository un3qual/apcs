package hw.L6.E9_8;

/**
 * The class for PE:9.8
 * @author Ryan Ramsdell
 */
public class Fan {

    private int speed = 1;
    private boolean on = false;
    private double radius = 5;
    private String color = "blue";

    public Fan() {

    }

    /**
     * Return object as string
     * @return
     */
    public String toString() {
        if (this.isOn()) {
            return "Speed: " + this.getSpeed() + "\n Color: " + this.getColor() + "\n Radius: " + this.getRadius();
        } else {
            return "Fan is off" + "\n Color: " + this.getColor() + "\n Radius: " + this.getRadius();
        }
    }

    /**
     * gets speed
     * @return
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * sets speed
     * @param speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * gets on or off
     * @return
     */
    public boolean isOn() {
        return on;
    }

    /**
     * sets on or off
     * @param on
     */
    public void setOn(boolean on) {
        this.on = on;
    }

    /**
     * gets radius
     * @return
     */
    public double getRadius() {
        return radius;
    }

    /**
     * sets radius
     * @param radius
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * gets color
     * @return
     */
    public String getColor() {
        return color;
    }

    /**
     * sets color
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

}
