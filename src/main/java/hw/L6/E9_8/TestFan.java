package hw.L6.E9_8;

/**
 * The test class for PE:9.8
 * @author Ryan Ramsdell
 */
public class TestFan {

    public static void main(String[] args) {
        // Create 2 fans
        Fan f1 = new Fan();
        Fan f2 = new Fan();
        // Set their params
        f1.setOn(true);
        f1.setColor("yellow");
        f1.setSpeed(3);
        f1.setRadius(10);

        f2.setSpeed(2);

        // Print them
        System.out.println(f1.toString());
        System.out.println(f2.toString());
    }

}
