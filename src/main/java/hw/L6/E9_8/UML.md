|       Fan       |
|:---------------:|
| speed           |
| color           |
| radius          |
| on              |
| --------------- |
| Fan()           |
| getSpeed()      |
| setSpeed()      |
| getColor()      |
| setColor()      |
| getRadius()     |
| setRadius()     |
| setOn()         |
| isOn()          |