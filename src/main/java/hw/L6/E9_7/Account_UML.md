|         Account         |
|:-----------------------:|
| uuid                    |
| balance                 |
| annualInterestRate      |
| dateCreated             |
| ----------------------- |
| Account()               |
| Account(_balance)       |
| get uuid                |
| get balance             |
| get annualInterestRate  |
| set balance             |
| set annualInterestRate  |
| get dateCreated         |
| get monthlyInterestRate |
| get monthlyInterest     |
| withdraw()              |
| deposit()               |