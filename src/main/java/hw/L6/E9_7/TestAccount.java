package hw.L6.E9_7;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TestAccount {

    public static void main(String[] args) {
        // To format dates nicly
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        // Create an account
        Account a = new Account(20000);
        // Set params
        a.setAnnualInterestRate(.045);
        a.withdraw(2500);
        a.deposit(3000);

        // Output the account info
        System.out.format(" Balance: $%.2f \n Monthly Interest: $%.2f \n Creation Date: %s", a.getBalance(), a.getMonthlyInterest(), df.format(a.getDateCreated()));
    }

}
