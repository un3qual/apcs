package hw.L6.E9_7;

import java.util.Date;
import java.util.UUID;

/**
 * The class for PE:9.7
 * @author Ryan Ramsdell
 */
public class Account {

    private UUID uuid;
    private double balance = 0;
    private double annualInterestRate = 0;
    private Date dateCreated;

    /**
     * no-arg Constructor
     */
    public Account() {
        this.uuid = UUID.randomUUID();
        this.dateCreated = new Date();
    }

    /**
     * Constructor with balance arg
     * @param _balance
     */
    public Account(double _balance) {
        this.uuid = UUID.randomUUID();
        this.dateCreated = new Date();
        this.balance = _balance;
    }

    /**
     * @return annual interest rate
     */
    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /**
     * @param annualInterestRate
     */
    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    /**
     * @return monthly interest rate
     */
    public double getMonthlyInterestRate() {
        return annualInterestRate / 12;
    }
    /**
     * @return monthly interest
     */
    public double getMonthlyInterest() {
        return this.balance * getMonthlyInterestRate();
    }
    /**
     * @return person's UUID
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @return balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * @return account creation date
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * Withdraws specified amount from account
     * @param amt
     */
    public void withdraw(double amt) {
        this.balance -= amt;
    }
    /**
     * Deposits specified amount into account
     * @param amt
     */
    public void deposit(double amt) {
        this.balance += amt;
    }


}
