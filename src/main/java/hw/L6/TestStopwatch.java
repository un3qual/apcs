package hw.L6;

import java.util.Scanner;

/**
 * The test class for PE:9.6
 * @author Ryan Ramsdell
 */
public class TestStopwatch {

    public static void selectionSort(double[] list) {
        for (int i = 0; i < list.length - 1; i++) {
            // Find the minimum in the list[i..list.length-1]
            double currentMin = list[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < list.length; j++) {
                if (currentMin > list[j]) {
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }

            // Swap list[i] with list[currentMinIndex] if necessary
            if (currentMinIndex != i) {
                list[currentMinIndex] = list[i];
                list[i] = currentMin;
            }
        }
    }
    /**
     * Basic Stopwatch implementation
     * @param args
     */
    public static void main(String[] args) {
        System.out.print("We will now sort an array \n");
        E9_6_Stopwatch sw = new E9_6_Stopwatch();
        double[] nums = new double[100000];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Math.random();
        }
        System.out.println("Started!!!");
        sw.start();
        selectionSort(nums);
        sw.stop();

        System.out.format("Stopped at %f seconds", sw.getElapsedTime() / 1000);
    }

}
