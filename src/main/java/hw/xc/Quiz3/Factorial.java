package hw.xc.Quiz3;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Factorial Class for Quiz 3 Extra Credit
 */
public class Factorial {

    /**
     * Calculates the factorial of a given double
     * @param num
     * @return num factorialized as double
     */
    public static double calcFactorial(double num) {

        double out = 1;

        for (double i = num; i > 0; i--) {
            out = (i < 1) ? calcFactorial(i) : out * i;
        }

        return (num == 0) ? 1 : out;

    }

    /**
     * Tells user the factorial of a number
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number to be factorialied: ");
        double num = input.nextDouble();
        System.out.format("%f! = %f", num, calcFactorial(num));
    }
}

