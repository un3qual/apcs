package hw.xc.Quiz3;


import java.util.Collections;
import java.util.Scanner;

/**
 * Interest Calculator Class for Quiz 3 Extra Credit
 */
public class InterestCalc {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter money loaned, interest rate, and number of days (double, double, int): ");
        double loaned = in.nextDouble();
        double rate = in.nextDouble();
        int days = in.nextInt();

        System.out.format("| Day   | Amount | Amt. Raw  |\n" +
                "|-------|--------|-----------|\n" +
                "| 0     | %.2f  | %f |\n", (double)Math.round(loaned), loaned);
        double currAmt = loaned;
        for (int i = 1; i < days + 1; i++) {

                    //System.out.println("Day: " + i + "Int: " + loaned * Math.pow(1.0 + (rate * .01), i));
            System.out.format("| %d %s | %.6f  | %f |\n", i, new String(new char[4 - ((int)Math.log10(i) + 1)]).replace("\0", " "), loaned * Math.pow(1.0 + (rate * .01), i), loaned * Math.pow(1.0 + (rate * .01), i));
        }
    }

}
