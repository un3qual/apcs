package hw.xc.Quiz4;

public class Utils {

    public static String rot47(String value) {

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);

            if (c != ' ') {
                c += 47;
                if (c > '~')
                    c -= 94;
            }

            result.append(c);
        }

        return result.toString();
    }

}
