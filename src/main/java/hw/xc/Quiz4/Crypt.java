package hw.xc.Quiz4;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/*

- each char to octal
- add key to each char
- each char to hexadeimal
- each char to string
- join all
- all base64
- rot47
- split into chars
- convert to ascii vals
- multiply by key
- join
- rot47
- base64
- base64
- return rot47


 */
public class Crypt extends CryptBase{



    public Crypt() {
        super("SAOL Encryption");

    }

    @Override
    public String encrypt(int key, String value) {
        // Convert vals to List of Strings
        List<String> valAsString = new ArrayList<String>( Arrays.asList(value.split("")) );
        // Init a List of chars for later conversion
        List<Character> valAsChars = new ArrayList<Character>();
        // Init a List of ints for ASCII later conversion
        List<Integer> valAsASCIIInts = new ArrayList<Integer>();
        List<Integer> valAsAsOctalInts = new ArrayList<Integer>();
        // Actually convert them
        for(String s : valAsString) {
            valAsChars.add(s.charAt(0));
        }
        // Make the chars into ints
        for(char c : valAsChars) {
            valAsASCIIInts.add((int) c);
        }
        for(int i : valAsASCIIInts) {
            valAsAsOctalInts.add(Integer.parseInt(Integer.toOctalString(i)));
        }
        List<Integer> forManip = new ArrayList<Integer>();
        int inc = 0;
        for(int i : valAsAsOctalInts) {
            // add key to vals
            forManip.add(valAsAsOctalInts.get(inc) + key);
            inc++;
        }
        inc = 0;
        // List for hex conversion
        List<String> valsAsHexString = new ArrayList<String>();
        inc = 0;
        for(int i : forManip) {
            valsAsHexString.add(Integer.toHexString(forManip.get(inc)));
            inc++;
        }
        String valsAfterHexConversion = String.join("", valsAsHexString);
        try {
            // Base64 it
            String base64EncodedV1 = Base64.getEncoder().encodeToString(valsAfterHexConversion.getBytes("utf-8"));
            // rot47 it
            String rot47V1 = Utils.rot47(base64EncodedV1);
            // split
            List<String> joinedToListPostEncode = new ArrayList<String>( Arrays.asList( rot47V1.split("") ) );
            List<Character> joinedToListPostEncodeAsChars = new ArrayList<Character>();
            for(String s : joinedToListPostEncode) {
                joinedToListPostEncodeAsChars.add(s.charAt(0));
            }
            List<Integer> joinedToListPostEncodeAsInts = new ArrayList<Integer>();
            for(char c : joinedToListPostEncodeAsChars) {
                // Make ASCII and multiply
                joinedToListPostEncodeAsInts.add(((int) c) * key);
            }
            List<String> joinedToListPostEncodeAsString = new ArrayList<String>();
            for(int i : joinedToListPostEncodeAsInts) {
                // Make ASCII and multiply
                joinedToListPostEncodeAsString.add(Integer.toString(i));
            }
            String joinedToListActuallyJoined = String.join("", joinedToListPostEncodeAsString);
            String rotV2 = Utils.rot47(joinedToListActuallyJoined);
            String base64V2 = Base64.getEncoder().encodeToString(rotV2.getBytes("utf-8"));
            String base64V3 = Base64.getEncoder().encodeToString(base64V2.getBytes("utf-8"));
            String rotV3 = Utils.rot47(base64V3);
            String base64V4 = Base64.getEncoder().encodeToString(rotV3.getBytes("utf-8"));


            return base64V4;


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "err";


    }

    @Override
    public String decrypt(int key, String value) {
        String base64V4 = new String(Base64.getDecoder().decode(value));
        String rotV3 = Utils.rot47(base64V4);
        String base64V3 = new String(Base64.getDecoder().decode(rotV3));
        String base64V2 = new String(Base64.getDecoder().decode(base64V3));
        String rotV2 = Utils.rot47(base64V2);
        //TODO: FINISH


        return "";
    }

}
