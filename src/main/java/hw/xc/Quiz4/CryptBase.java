package hw.xc.Quiz4;

public class CryptBase {
    private String CrypterTitle;

    public CryptBase(String _crypterTitle) {
        this.CrypterTitle = _crypterTitle;
    }
    public String encrypt(int key, String value) {
        return value + " encrypted with " + CrypterTitle + " using the key " + key + " is " + value;
    }
    public String decrypt(int key, String value) {
        return value + " decrypted with " + CrypterTitle + " using the key " + key + " is " + value;
    }
}
