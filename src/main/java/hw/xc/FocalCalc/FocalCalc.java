package hw.xc.FocalCalc;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import static java.awt.Color.decode;

/**
 * Calculates focal length
 */

public class FocalCalc extends JFrame {
    private static final int WIDTH = 400;
    private static final int HEIGHT = 300;

    private JLabel objDistLB, imgDistLB, focalLenLB;
    private JTextField objDistTF, imgDistTF, focalLenTF;
    private JButton calculateB;
    public static final Color disabledFormColor = new Color(0xF5F5F5);

    //Button handlers
    private CalculateButtonHandler cbHandler;

    public FocalCalc() {
        objDistLB = new JLabel("Object distance: ", SwingConstants.RIGHT);
        imgDistLB = new JLabel("Image distance: ", SwingConstants.RIGHT);
        focalLenLB = new JLabel("Focal Length", SwingConstants.RIGHT);

        objDistTF = new JTextField(10);
        imgDistTF = new JTextField(10);
        focalLenTF = new JTextField(10);
        focalLenTF.setEditable(false);
        focalLenTF.setBackground(disabledFormColor);

        //Specify handlers for each button and register ActionListeners to each button.
        calculateB = new JButton("Calculate");
        cbHandler = new CalculateButtonHandler();
        calculateB.addActionListener(cbHandler);

        setTitle("Calculate Lens Stuff");
        Container pane = getContentPane();
        pane.setLayout(new GridLayout(3, 2));

        //Add the stuff to the pane
        pane.add(objDistLB);
        pane.add(imgDistLB);
        pane.add(focalLenLB);
        pane.add(objDistTF);
        pane.add(imgDistTF);
        pane.add(focalLenTF);
        pane.add(calculateB);

        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private class CalculateButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double objDist, imgDist, focalLen;

            objDist = Double.parseDouble(objDistTF.getText());
            imgDist = Double.parseDouble(imgDistTF.getText());
            focalLen = objDist + imgDist;

            focalLenTF.setText("" + focalLen);
        }
    }

    public class ExitButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }


    /**
     * Created new instance of FocalCalc
     * @param args
     */
}