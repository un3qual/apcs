package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.5
 * @author Ryan Ramsdell
 */
public class E5_CalcTips {

    /**
     * Calculate tips
     * @param args
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a subtotal: ");
        double subTotal = input.nextDouble();

        System.out.print("Enter a gratuity rate: ");
        double tipPercent = input.nextDouble();


        double tip = subTotal * (tipPercent / 100);
        double total = subTotal + tip;
        System.out.format("A $%f bill with a %f percent tip totals to $%f", subTotal, tipPercent, total);
    }

}
