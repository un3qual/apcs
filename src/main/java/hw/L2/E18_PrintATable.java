package hw.L2;

/**
 * The class for PE:2.12
 * @author Ryan Ramsdell
 */
public class E18_PrintATable {

    /**
     * Prints a table
     * @param args
     */
    public static void main(String[] args) {

        double[] col1 = new double[5];
        double[] col2 = new double[5];
        double[] col3 = new double[5];

        System.out.println("A     B     pow(A,B)");
        for (int i = 0; i < 5; i++) {
            col1[i] = i + 1;
            col2[i] = i + 2;
            col3[i] = Math.pow(col1[i], col2[i]);
            System.out.format("%d     %d     %d\n", (int)col1[i], (int)col2[i], (int)col3[i]);
        }

    }

}
