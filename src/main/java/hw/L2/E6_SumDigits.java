package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.6
 * @author Ryan Ramsdell
 */
public class E6_SumDigits {

    /**
     * Calculates the sum of digits of a given int
     * @param args
     */
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer: ");

        int number = input.nextInt();
        final int numberIn = number;
        int sum = 0;
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        System.out.format("The sum of the digits of %d is %d", numberIn, sum);

    }

}
