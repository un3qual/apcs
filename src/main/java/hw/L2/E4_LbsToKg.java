package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.4
 * @author Ryan Ramsdell
 */
public class E4_LbsToKg {

    /**
     * Converts lbs to kg
     * @param args
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number of pounds to be converted to kilograms: ");

        double pounds = input.nextDouble();
        double kilograms = pounds * 0.454;
        System.out.println(pounds + "lbs is " + kilograms + " kg.");
    }

}
