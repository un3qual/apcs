package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.12
 * @author Ryan Ramsdell
 */
public class E12_RunwayLength {


    /**
     * Calculates runway length
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the plane's take-off speed (m/s): ");
        final double takeOffSpeed = input.nextDouble();
        System.out.print("Enter the plane's acceleration (m/s/s): ");
        final double accel = input.nextDouble();

        final double runwayLength = (Math.pow(takeOffSpeed, 2)) / (2 * accel);

        System.out.format("The minium runway length for a plane that took off with an acceleration of %fm/s/s and a takeoff speed of %fm/s is %f meters.", accel, takeOffSpeed, runwayLength);

    }

}
