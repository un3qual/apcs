package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.23
 * @author Ryan Ramsdell
 */
public class E23_CostOfDriving {

    /**
     * Calculates cost of driving.
     * @param args
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter the driving distance, mpg, and price per gallon: ");

        final double distance = input.nextDouble();
        final double mpg = input.nextDouble();
        final double price = input.nextDouble();

        final double gals = distance / mpg;
        final double totalCost = gals * price;

        System.out.format("The cost of driving %f miles will cost $%f", distance, totalCost);

    }

}
