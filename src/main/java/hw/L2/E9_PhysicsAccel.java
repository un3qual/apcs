package hw.L2;

/**
 * The class for PE:2.9
 * @author Ryan Ramsdell
 */
import java.util.Scanner;

public class E9_PhysicsAccel {

    /**
     * Calculates acceleration given a starting/ending velocity and a time
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the starting velocity (in m/s): ");
        final double startingVelocity = input.nextDouble();
        System.out.print("Enter the ending velocity (in m/s): ");
        final double endingVelocity = input.nextDouble();
        System.out.print("Enter the time (in seconds): ");
        final double time = input.nextDouble();

        final double accel = (endingVelocity - startingVelocity) / time;

        System.out.format("The average acceleration for an object starting at %fm/s and ending at %fm/s over %f seconds is %fm/s/s", startingVelocity, endingVelocity, time, accel);



    }

}
