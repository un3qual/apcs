package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.7
 * @author Ryan Ramsdell
 */
public class E7_NumOfYears {


    /**
     * Converts minutes to years and days
     * @param args
     */
    public static void main(String[] args){

        final int minPerYear = (60 * 24 * 365);
        final int minPerDay = (60 * 24);
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of minutes: ");
        int minutes = input.nextInt();
        int years = minutes / minPerYear;
        int days = (minutes % minPerYear) / minPerDay;
        System.out.format("%d minutes is approximately %d years and %d days", minutes, years, days);
    }

}
