package hw.L2;

/**
 * The class for PE:2.2
 * @author Ryan Ramsdell
 */

import java.util.Scanner;

public class E2_VolOfCyl {

    /**
     * Calculates radius of cylinder
     * @param args
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the radius of a cylinder: ");
        double radius = input.nextDouble();
        System.out.print("Enter the length of the cylinder: ");
        double length = input.nextDouble();

        double area = Math.pow(radius, 2) * Math.PI;
        double volume = area * length;
        System.out.println("The area of a cylinder with a radius of " + radius + " and a length of " +
        length + " is " + area + " square units.");
        System.out.println("The volume of the cylinder is " + volume + " units cubed.");
    }

}
