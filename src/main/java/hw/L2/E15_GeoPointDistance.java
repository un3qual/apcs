package hw.L2;

/**
 * The class for PE:2.12
 * @author Ryan Ramsdell
 */
import java.util.Scanner;

public class E15_GeoPointDistance {

    /**
     * Calculates distance between two given points
     * @param args
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter one point: ");
        final double x1 = input.nextDouble();
        final double y1 = input.nextDouble();
        System.out.print("Enter the second point: ");
        final double x2 = input.nextDouble();
        final double y2 = input.nextDouble();

        final double distance = Math.pow(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2), 0.5);

        System.out.format("The distance between (%f, %f) and (%f, %f) is %f", x1, y1, x2, y2, distance);



    }

}
