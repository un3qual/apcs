package hw.L2;

import java.util.Scanner;

/**
 * The class for PE:2.3
 * @author Ryan Ramsdell
 */

public class E3_FeetToMeters {

    /**
     * Converts feet to meters
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number of feet to be converted to meters: ");

        double feet = input.nextDouble();
        double meters = feet * 0.305;
        System.out.println(feet + " feet is " + meters + " meters.");
    }

}
