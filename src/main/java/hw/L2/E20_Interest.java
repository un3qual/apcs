package hw.L2;

/**
 * The class for PE:2.20
 * @author Ryan Ramsdell
 */
import java.util.Scanner;

public class E20_Interest {

    /**
     * Calculates interest
     * @param args
     */
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        System.out.print("Enter balance and interest rate (e.g., 3 for 3%): ");
        final double balance = input.nextDouble();
        final double interestRateAnnual = input.nextDouble();
        final double interest = balance * (interestRateAnnual / 1200);

        System.out.format("%f percent interest on %f for the next month is %f.", interestRateAnnual, balance, interest);

    }

}
