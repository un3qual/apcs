package hw.L2;

/**
 * The class for PE:2.1
 * @author Ryan Ramsdell
 */
import java.util.Scanner;

public class E1_C_To_F {

    /**
     * Converts C to F
     * @param args
     */
    public static void main(String[] args) {
        System.out.print("Enter a temperature in celsius: ");
        Scanner input = new Scanner(System.in);

        double cInput = input.nextDouble();
        double fahrenheit = (9 / 5) * cInput + 32;
        System.out.println(cInput + "C is " + fahrenheit + "F");
    }
}
