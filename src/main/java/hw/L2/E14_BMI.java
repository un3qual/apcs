package hw.L2;

/**
 * The class for PE:2.14
 * @author Ryan Ramsdell
 */
import java.util.Scanner;

public class E14_BMI {

    /**
     * Calculates BMI
     * @param args
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter your weight in pounds: ");
        final double weightInLbs = input.nextDouble();
        System.out.print("Enter your height in inches: ");
        final double heightInInches = input.nextDouble();

        // Convert measurements to metric
        final double weightInKg = weightInLbs * 0.45359237;
        final double heightInM = heightInInches * 0.0254;

        final double BMI = (weightInKg) / (Math.pow(heightInM, 2));

        System.out.format("The BMI of a person weighing %f lbs and %f inches tall is %f.", weightInLbs, heightInInches, BMI);


    }
}
