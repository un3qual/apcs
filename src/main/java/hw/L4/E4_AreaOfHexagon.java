package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.4
 * @author Ryan Ramsdell
 */
public class E4_AreaOfHexagon {

    /**
     * Calculates area of a hexagon given side length
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the length of a side of a hexagon: ");
        double sideLength = input.nextDouble();
        double area = (6 * Math.pow(sideLength, 2)) / (4 * Math.tan(Math.PI / 6));
        System.out.format("The area of the hexagon is %f units^2", area);
    }

}
