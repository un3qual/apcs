package hw.L4;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class for PE:4.21
 * @author Ryan Ramsdell
 */
public class E21_CheckSSN {

    /**
     * Validates SSN using regex
     * @param args
     */
    public static void main(String[] args) {

        String SSN_REGEX = "^(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$";
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a Social Security Number to validate: ");
        String ssn = input.nextLine();
        Pattern patVal = Pattern.compile(SSN_REGEX);
        Matcher patMatch = patVal.matcher(ssn);

        if(patMatch.matches() == true) {
            System.out.format("%s is a valid SSN.", ssn);
        } else {
            System.out.format("%s is not a valid SSN", ssn);
        }

    }

}
