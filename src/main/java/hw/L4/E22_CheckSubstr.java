package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.22
 * @author Ryan Ramsdell
 */
public class E22_CheckSubstr {

    /**
     * Returns if a string contains another string and where.
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first string: ");
        String s1 = input.nextLine();
        System.out.print("Enter the second string: ");
        String s2 = input.nextLine();


        if (s1.equals(s2)){
            System.out.format("%s == %s", s1, s2);
        } else if(s1.contains(s2) && !s1.equals(s2)) {
            String[] splitStringParts = s1.split(s2);
            System.out.format("%s contains %s at: %s{%s}%s.", s1, s2, splitStringParts[0], s2, (splitStringParts.length < 2) ? "" : splitStringParts[1]);
        } else {
            System.out.format("%s is not contained anywhere in %s", s1, s2);
        }
    }

}