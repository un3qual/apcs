package hw.L4;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.CancellationException;

/**
 * The class for PE:4.17
 * @author Ryan Ramsdel
 */
public class E17_DaysOfAMonth {

    /**
     * Gets number of days in a month given a month
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a year: ");
        int year = input.nextInt();
        System.out.print("Enter a month: ");
        String month = input.next();
        String cleanMonth = month.substring(0, 1).toUpperCase() + month.substring(1);
        String[] monthAbbr = new DateFormatSymbols().getShortMonths();
        ArrayList<String> monthArray = new ArrayList<String>();
        for (int i = 0; i < monthAbbr.length; i++) {
            monthArray.add(i, monthAbbr[i]);
        }
        if (!monthArray.contains(cleanMonth)) {
            System.out.format("%s is not a valid shortMonth!", month);
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, (monthArray.indexOf(cleanMonth)));

            System.out.format("%s %d has %d days.", new DateFormatSymbols().getMonths()[Calendar.MONTH-2], year, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
    }

}
