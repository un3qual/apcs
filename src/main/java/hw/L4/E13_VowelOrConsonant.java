package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.8
 * @author Ryan Ramsdell
 */
public class E13_VowelOrConsonant {

    /**
     * Is it a vowel???
     * @param c
     * @return
     */
    public static boolean isVowel(char c) {
        return "AEIOUaeiou".indexOf(c) != -1;
    }

    /**
     * Outputs whether or not a char is a vowel
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a letter: ");
        char gets = input.next().charAt(0);
        if(!Character.isLetter(gets))
            System.out.format("%s is not a letter!", gets);
        else {

            System.out.format("%s is a %s", gets, (isVowel(gets)) ? "Vowel" : "Consonant");
        }

    }



}
