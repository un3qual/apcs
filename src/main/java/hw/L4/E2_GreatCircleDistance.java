package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.2
 * @author Ryan Ramsdell
 */
public class E2_GreatCircleDistance {

    /**
     * Calculates the distance between two points on Earth
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter 2 points (latitude and longitude) in degrees: ");
        double x1 = Math.toRadians(input.nextDouble());
        double y1 = Math.toRadians(input.nextDouble());
        double x2 = Math.toRadians(input.nextDouble());
        double y2 = Math.toRadians(input.nextDouble());
        final double radius = 6371.01;
        double gcd = radius * (Math.acos( (Math.sin(x1) * Math.sin(x2)) + (Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2)) ));
        System.out.format("The distance between the two points is %fkm", gcd);
    }


}
