package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.1
 * @author Ryan Ramsdell
 */
public class E1_RegPolygonArea {

    /**
     * Calculates the area of a pentagon
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the length from the center to a vertex and the number of sides: ");
        double radius = input.nextDouble();
        int sideCount = input.nextInt();

        double sideLength = (2 * radius) * (Math.sin(Math.PI / sideCount));
        double area = (5 * Math.pow(sideLength, 2)) / (4 * (Math.tan(Math.PI / sideCount)));
        double roundedArea = Math.round(area * 100.0d) / 100.0d;

        System.out.format("\n The area of a regular polygon with %d sides and a radius of %f is %f units^2", sideCount, radius, roundedArea);

    }

}
