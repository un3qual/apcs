package hw.L4;

import java.util.Scanner;

/**
 * The class for PE:4.8
 * @author Ryan Ramsdell
 */
public class E8_CharFromASCII {

    /**
     * Finds the character of an ASCII code.
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an ASCII code: ");
        int asciiCode = input.nextInt();
        System.out.format("The ASCII code %d is actually %s", asciiCode, Character.toString((char)asciiCode));
    }

}
