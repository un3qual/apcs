package hw.L4;

import java.util.Random;
import java.util.Scanner;

/**
 * The class for PE:4.25
 * @author Ryan Ramsdell
 */
public class E25_GenLicensePlate {

    /**
     * Generates a random license plate
     * @param args
     */
    public static void main(String[] args) {
        Random rand = new Random();
        String validAlphabet = "ABCDEFGHJKLMNPQRTUVWXYZ";

        System.out.println("Now generating a License Plate");
        String letters = "";
        for (int i = 0; i < 3; i++) {
            letters += validAlphabet.charAt(rand.nextInt((validAlphabet.length() - 0) + 1) - 0);
        }
        String nums = "";
        for (int i = 0; i < 4; i++) {
            nums += Integer.toString(rand.nextInt((9 - 0) + 1) - 0);
        }
        String plate = letters + nums;
        System.out.format("Your license plate is: %s", plate);




    }

}
