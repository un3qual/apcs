package hw.L4;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The class for PE:4.24
 * @author Ryan Ramsdell
 */
public class E24_OrderingCities {

    /**
     * Orders a list of cities
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a comma separated list of cities (no spaces between) \n i.e: Citie,citie,Citie: ");
        String joinedPre = input.nextLine();
        String[] splitCities = joinedPre.split(",");
        Arrays.sort(splitCities);
        String finalCities = String.join(", ", splitCities);
        System.out.println("Your alphabetized list of cities is: \n" + finalCities);
    }
}
