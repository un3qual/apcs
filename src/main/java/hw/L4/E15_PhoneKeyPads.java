package hw.L4;

import java.util.*;

/**
 * The class for PE:4.15
 * @author Ryan Ramsdel
 */
public class E15_PhoneKeyPads {

    /**
     * Converts letter to phone keypad number
     * @param args
     */
    public static void main(String[] args) {
        // Create table for the nums and their letter values
        Hashtable<Integer, String> assoc = new Hashtable<Integer, String>();
        assoc.put(2, "abc");
        assoc.put(3, "def");
        assoc.put(4, "ghi");
        assoc.put(5, "jkl");
        assoc.put(6, "mno");
        assoc.put(7, "pqrs");
        assoc.put(8, "tuv");
        assoc.put(9, "wxyz");
        // Scanner
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a phone number: ");
        String phoneNum = input.nextLine();
        // Convert user inputted string to Array
        String[] phoneArray = phoneNum.toLowerCase().split("");

        // Init List for the final output
        List<String> formattedPhone = new ArrayList<String>();
        // Incrementer for a foreach
        int i = 1;
        for (String ins : phoneArray) {
            // Increment incrementer
            i++;
            // If we need to convert this char from a letter to number
            if (Character.isLetter(ins.charAt(0))) {
                // Loop through the numbers and look for the number for this letter
                for (int key : assoc.keySet()) {
                    // One last check to make sure this is a valid letter
                    if (assoc.get(key).indexOf(ins) != -1) {
                        // Append this appropriate num to the phone number List
                        formattedPhone.add(Integer.toString(key));
                    }
                }
            } else {
                // If not a letter, just add the char to the end of the List for the final phone number
                formattedPhone.add(Character.toString(ins.charAt(0)));
            }
        }
        // Print the final phone number and join the List into a String
        System.out.format("The valid phone number is %s", String.join("", formattedPhone));


    }
}
