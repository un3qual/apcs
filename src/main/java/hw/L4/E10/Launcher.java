package hw.L4.E10;

/**
 * Launcher for PE:4.10
 * @author Ryan Ramsdell
 */
public class Launcher {

    /**
     * Launches the app
     * @param args
     */
    public static void main(String[] args) {
        E10_BDayGUIManager app = new E10_BDayGUIManager();
    }

}
