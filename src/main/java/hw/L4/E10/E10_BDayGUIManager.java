package hw.L4.E10;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * GUI class for PE:4.10
 */
public class E10_BDayGUIManager extends JFrame{
    private JPanel rootPanel;
    private JButton calcBtn;
    private JPanel calcPanel;
    private JLabel imgLabel;
    private JButton btn1;
    private JButton btn5;
    private JButton btn2;
    private JButton btn3;
    private JButton btn4;
    private int bDay = 0;
    private int firstNums[] = {1, 2, 4, 8, 16};


    // Constructor
    public E10_BDayGUIManager(){
        super("BDayGUIManager");
        setContentPane(rootPanel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        calcBtn.setEnabled(false);


//        ImageIcon sets = new ImageIcon("set_sprite.png");
//        imgLabel = new JLabel("", sets, JLabel.CENTER);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bDay += firstNums[0];
                btn1.setEnabled(false);
                calcBtn.setEnabled(true);
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bDay += firstNums[1];
                btn2.setEnabled(false);
                calcBtn.setEnabled(true);
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bDay += firstNums[2];
                btn3.setEnabled(false);
                calcBtn.setEnabled(true);
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bDay += firstNums[3];
                btn4.setEnabled(false);
                calcBtn.setEnabled(true);
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bDay += firstNums[4];
                btn5.setEnabled(false);
                calcBtn.setEnabled(true);
            }
        });
        calcBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(new JFrame(), "The day you were born is: " + bDay, "Your Birthday",
                        JOptionPane.INFORMATION_MESSAGE);
                btn1.setEnabled(true);
                btn2.setEnabled(true);
                btn3.setEnabled(true);
                btn4.setEnabled(true);
                btn5.setEnabled(true);
                bDay = 0;
                calcBtn.setEnabled(false);

            }
        });


                setVisible(true);
            }
        }
