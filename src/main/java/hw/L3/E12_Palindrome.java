package hw.L3;

import java.util.Scanner;

/**
 * Class for PE3.18
 * @author Ryan Ramsdell
 */
public class E12_Palindrome {

    /**
     * Tells you if a word is a palindrome
     * @param args
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter something to find out if it is a palindrome: ");
        String thing = input.next();
        if(thing.equals(new StringBuilder(thing).reverse().toString())) {
           System.out.format("%s reversed is %s, therefore it is a palindrome! :D", thing,
           new StringBuilder(thing).reverse().toString());
        } else {
            System.out.format("%s reversed is %s, therefore it is not a palindrome! :(", thing,
                    new StringBuilder(thing).reverse().toString());
        }
    }

}

