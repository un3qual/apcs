package hw.L3.E14;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormatSymbols;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * The GUI class for PE:3.14
 * @author Ryan Ramsdell
 */
public class HeadsOrTails extends JFrame{
    private JButton tailsButton;
    private JButton headsButton;
    private JPanel rootPanel;
    private Random rand;

    public HeadsOrTails() {
        super("HeadsOrTails");
        setContentPane(rootPanel);
        rand = new Random();

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        /**
         * Executed when the Tails button is pressed
         */
        tailsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean result = genCoin();
                if (result == true) {
                    JOptionPane.showMessageDialog(new JFrame(), "TAILS. You win! :D", "You Lose",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), "HEADS. You lose! xD", "You Lose",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        /**
         * Executed when the Heads button is pressed
         */
        headsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean result = genCoin();
                if (result == true) {
                    JOptionPane.showMessageDialog(new JFrame(), "TAILS. Sorry, you lose. xD", "You Lose",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), "HEADS. You win! :D", "You Win",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        setVisible(true);
    }

    public boolean genCoin(){

        return rand.nextBoolean();
    }


}
