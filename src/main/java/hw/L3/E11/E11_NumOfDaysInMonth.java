package hw.L3.E11;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * The GUI class for PE:3.11
 * @author Ryan Ramsdell
 */
public class E11_NumOfDaysInMonth extends JFrame {
    private JTextField yearTF;
    private JButton calculateButton;
    private JLabel resultLabel;
    private JPanel rootPanel;
    private JLabel monthLabel;
    private JLabel yearLabel;
    private JComboBox monthPicker;
    Calendar calendar = Calendar.getInstance();
    String[] monthList = new DateFormatSymbols().getMonths();

    public E11_NumOfDaysInMonth() {
        super("MonthCalc");
        setContentPane(rootPanel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        /**
         * Executed when the Calculate button is pressed
         */
        calculateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GregorianCalendar gc = new GregorianCalendar();
                int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

                int month = monthPicker.getSelectedIndex();
                int year = Integer.parseInt(yearTF.getText());

                if (yearTF.getText() == null) {
                    JOptionPane.showMessageDialog(new JFrame(), "All forms must be filled in", "Dialog",
                            JOptionPane.ERROR_MESSAGE);
                } else if (year < 0) {
                    JOptionPane.showMessageDialog(new JFrame(), "Invalid year", "Dialog",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    gc.set(gc.YEAR, year);
                    gc.set(gc.MONTH, month);
                    daysInMonths[1] = gc.isLeapYear(gc.get(gc.YEAR)) ? 29 : 28;
                    int days = daysInMonths[month];
                    resultLabel.setText("There are " + days + " days in " + new DateFormatSymbols().getMonths()[month] + " " + year);
                }


            }
        });

        setVisible(true);
    }

}
