package hw.L3;

import java.awt.*;
import java.util.Scanner;

public class E28_GeoTwoRectangles {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter r1's center x-, y-coordinates, width, and height: ");
        double r1CenterX = input.nextDouble();
        double r1CenterY = input.nextDouble();
        double r1Width = input.nextDouble();
        double r1Height = input.nextDouble();
        double r1TopLeftX = r1CenterX - (r1Width / 2);
        double r1TopLeftY = r1CenterY + (r1Height / 2);
        Rectangle r1 = new Rectangle((int)r1TopLeftX, (int)r1TopLeftY, (int)r1Width, (int)r1Height);
        System.out.print("Enter r2's center x-, y-coordinates, width, and height: ");
        double r2CenterX = input.nextDouble();
        double r2CenterY = input.nextDouble();
        double r2Width = input.nextDouble();
        double r2Height = input.nextDouble();
        double r2TopLeftX = r2CenterX - (r2Width / 2);
        double r2TopLeftY = r2CenterY + (r2Height / 2);
        Rectangle r2 = new Rectangle((int)r2TopLeftX, (int)r2TopLeftY, (int)r2Width, (int)r2Height);



        if (r1.intersects(r2) && r2.intersects(r1)) {
            if (!r1.contains(r2) && !r2.contains(r1))
                System.out.println("The two Rectangles intersect.");
        } else if (r1.contains(r2)) {
            if (!r1.intersects(r2) && !r2.intersects(r1))
                System.out.println("Rectangle 1 contains Rectangle 2");
        } else if (r2.contains(r1)) {
            if (!r1.intersects(r2) && !r2.intersects(r1))
                System.out.println("Rectangle 2 contains Rectangle 1");
        } else {
            System.out.println("The two Rectangles do not intersect");
        }





    }

}
