package hw.L3;

import java.text.DateFormatSymbols;
import java.util.Scanner;

/**
 * The class for PE3.21
 * @author Ryan Ramsdell
 */
public class E21_DayOfWeek {

    /**
     * Calculates day of week using Zeller's Congruence
     * @param args
     */
    public static void main(String[] args) {

        String days[] = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday"};
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the day of month, month, and the year \n (i.e 14 4 2015): ");

        int dayOfMonth = input.nextInt();
        int month = input.nextInt();
        int year = input.nextInt();
        int century = year / 100;
        int yearOfCentury = year % 100;

        int dayOfWeek = (dayOfMonth + ((26 * (month + 1)) / 10) + yearOfCentury + (yearOfCentury / 4) + (century / 4) + (5 * century)) % 7;
        int result = (dayOfWeek == 6) ? 0 : dayOfWeek;
        System.out.format("The day of the week is %s", days[result]);
    }

}
