package hw.L3;

import com.sun.deploy.util.StringUtils;

import java.util.*;

/**
 * The class for PE:3.8
 * @author Ryan Ramsdell
 */
public class E8_SortThreeInts {

    /**
     * Sorts ints
     * @param args
     */
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<Integer>();
        Scanner input = new Scanner(System.in);
        System.out.print("Enter some integers (end sequence with a non int): ");

        while (input.hasNextInt()) {
            numbers.add(input.nextInt());
        }
        Collections.sort(numbers);

        StringBuilder sb = new StringBuilder();
        for (Integer num : numbers) {
            sb.append(num != null ? num.toString() + ", " : "");
        }

        System.out.println(sb.toString());
    }
    
}
