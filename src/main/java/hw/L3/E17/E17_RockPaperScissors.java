package hw.L3.E17;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * GUI class for PE 3.17
 * @author Ryan Ramsdell
 */
public class E17_RockPaperScissors extends JFrame{
    private JButton rockButton;
    private JButton paperButton;
    private JButton scissorsButton;
    private JPanel rootPanel;
    private Random random;

    // Constructer
    public E17_RockPaperScissors() {
        super("RockPaperScissors");
        setContentPane(rootPanel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        random = new Random();


        /**
         * Executed when the Rock button is pressed
         */
        rockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = genMove();
                switch (result) {
                    case 0:
                        JOptionPane.showMessageDialog(new JFrame(), "Tie. Try again later.", "Tie",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 1:
                        JOptionPane.showMessageDialog(new JFrame(), "Paper beats rock. You lose!", "Loser!",
                                JOptionPane.WARNING_MESSAGE);
                        break;
                    case 2:
                        JOptionPane.showMessageDialog(new JFrame(), "Rock beats scissors. You win :D", "Winner!",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                }

            }
        });
        /**
         * Executed when the Paper button is pressed
         */
        paperButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = genMove();
                switch (result) {
                    case 0:
                        JOptionPane.showMessageDialog(new JFrame(), "Paper beats rock. You win :D", "Winner!",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 1:
                        JOptionPane.showMessageDialog(new JFrame(), "Tie. Try again later.", "Tie",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 2:
                        JOptionPane.showMessageDialog(new JFrame(), "Scissors beat paper. You lose!", "Loser!",
                                JOptionPane.WARNING_MESSAGE);
                        break;
                }
            }
        });

        /**
         * Executed when the scissors button is pressed
         */
        scissorsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = genMove();
                switch (result) {
                    case 0:
                        JOptionPane.showMessageDialog(new JFrame(), "Rock beats scissors. You lose!", "Loser!",
                                JOptionPane.WARNING_MESSAGE);
                        break;
                    case 1:
                        JOptionPane.showMessageDialog(new JFrame(), "Scissors beats paper. You win :D", "Winner!",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 2:

                        JOptionPane.showMessageDialog(new JFrame(), "Tie. Try again later.", "Tie",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
        });

        setVisible(true);
    }

    public int genMove(){

        int num = random.nextInt(3);

        return num;

    }

}
