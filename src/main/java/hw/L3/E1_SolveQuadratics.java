package hw.L3;

import java.util.Scanner;

/**
 * The class for PE:3.1
 * @author Ryan Ramsdell
 */
public class E1_SolveQuadratics {

    /**
     * Solves quadratics
     * @param args
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a, b, and c: ");
        double a = input.nextDouble();
        double b = input.nextDouble();
        double c = input.nextDouble();

        double discriminant = Math.pow(b, 2) - (4 * a * c);

        if(discriminant < 0) {
            System.out.println("The equation has no real roots.");
        }

        if(discriminant >= 0 ) {
            double posRoot = ((b * -1) + Math.sqrt(discriminant)) / (2 * a);
            double negRoot = ((b * -1) - Math.sqrt(discriminant)) / (2 * a);
            System.out.format("The equation has two roots %f and %f", posRoot, negRoot);
            if (posRoot == negRoot) {
                System.out.format("The equation has one root %f.", posRoot);
            }
        }


    }

}
