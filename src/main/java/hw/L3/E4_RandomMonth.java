package hw.L3;

import java.text.DateFormatSymbols;
import java.util.Scanner;

/**
 * The class for PE:3.4
 * @author Ryan Ramsdell
 */
public class E4_RandomMonth {

    /**
     * Gets month name from an Int
     * @param args
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a number of a month: ");
        int monthIndex = input.nextInt();
        String[] monthList = new DateFormatSymbols().getMonths();

        if (monthIndex > 12 || monthIndex < 1) {
            System.out.println("Invalid Month!");
            System.out.println("Enter a number of a month: ");
            monthIndex = input.nextInt();
        } else {
            System.out.println(monthList[monthIndex - 1]);
        }

    }

}
