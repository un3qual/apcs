package hw.L3;

import java.util.Scanner;

/**
 * The class for PE
 */
public class E18_Shipping {

    /**
     * Calculates price of package based on weight
     * @param args
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter package weight (no decimals): ");
        int weight = input.nextInt();
        if (weight > 0 && weight <= 1) {
            System.out.println("$3.50");
        } else if (weight > 1 && weight <= 3) {
            System.out.println("$5.50");
        } else if (weight > 3 && weight <= 10) {
            System.out.println("$8.50");
        } else if (weight > 10 && weight <= 20) {
            System.out.println("$10.50");
        } else {
            System.out.println("Sorry. We can't ship this package.");
        }
    }

}
