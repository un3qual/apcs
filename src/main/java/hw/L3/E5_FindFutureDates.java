package hw.L3;

import java.text.DateFormatSymbols;
import java.util.Scanner;

/**
 * The class for PE:3.5
 * @author Ryan Ramsdell
 */
public class E5_FindFutureDates {

    /**
     * Prints day from days elapsed
     * @param args
     */
    public static void main(String[] args) {
        String[] days = new DateFormatSymbols().getWeekdays();

        Scanner input = new Scanner(System.in);
        System.out.println("Enter today's date (number): ");
        int today = input.nextInt();
        System.out.println("Enter the number of days elapsed since today: ");
        int daysElapsed = input.nextInt();

        int day = (daysElapsed % 7) + today;
        String dayName = days[day + 1];
        System.out.println(dayName);


    }

}

