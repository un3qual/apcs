package hw.L3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


/**
 * The class for PE:3.8
 * @author Ryan Ramsdell
 */
public class E9_ISBN {

    /**
     * Calculates checkum from ISBN (ignores X)
     * @param ISBNWithoutChecksum
     * @return checksum
     */
    public static String checksumCalc(String ISBNWithoutChecksum){
        int total = 0;
        for ( int i = 0; i < 9; i++ )
        {
            int digit = Integer.parseInt(ISBNWithoutChecksum.substring(i, i + 1));
            total += ((10 - i) * digit);
        }
        String checksum = Integer.toString((11 - (total % 11)) % 11);
        return checksum;
    }

    /**
     * Calculates ISBN checksum
     * @param args
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first 9 digits of an ISBN: ");
        String first9OfISBN = input.nextLine();
        int checksum = Integer.parseInt(checksumCalc(first9OfISBN));
        if (checksum == 10) {
            System.out.println(first9OfISBN + "X");
        } else {
            System.out.println(first9OfISBN + checksum);
        }




    }

}
