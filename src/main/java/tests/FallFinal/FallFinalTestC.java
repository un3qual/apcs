package tests.FallFinal;

import java.util.Arrays;

public class FallFinalTestC {

    /**
     * Find the minimum value in an array of doubles
     * @param a
     * @return
     */
    public static double minValInArray(double[] a) {
        // Set min to the first value in "a"
        double min = a[0];

        // Loop through the array "a"
        for (int i = 0; i < a.length; i++) {
            // If the value for this index of "a" is less than the current minimum
            if(a[i] < min)
                // Set the new minimum to the current value for this index of "a"
                min = a[i];
        }
        // Return the minimum
        return min;
    }

    /**
     * Find the minimum value in an array of doubles
     * @param a The array
     * @return The smallest value in "a"
     */
    public static double minValInArrayMethod2(double[] a) {
        // Sort the array from smallest to largest
        Arrays.sort(a);
        // Return the first value
        return a[0];
    }
    public static void main(String[] args) {
        double[] test1 = {4, 5, 1, 4, 5, 4};
        double[] test2 = {6.4, 8.2, .24, 2.44, .4234, 132.2, 3};

        System.out.format("The minimum value in test1 is %f \n", minValInArray(test1));
        System.out.format("The minimum value in test2 is %f", minValInArrayMethod2(test2));
    }
}
